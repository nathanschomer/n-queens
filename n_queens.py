from async_backtrack import AsyncBacktrack
import time
from argparse import ArgumentParser
from gameboard import GameBoard


def check_world_state(agents, show=False):

    gb = GameBoard(len(agents), empty=True)

    stop = False

    for agent in agents:
        gb.queens[agent.priority] = agent.agent_view.queens[agent.priority]
        if agent.cont is False:
            stop = True

    if show:
        print(gb)

    return gb.is_valid(), stop


def run_threaded(n_queens):

    threads = []
    is_valid = False
    no_sol = False

    for n in range(n_queens):
        threads.append(AsyncBacktrack(n_queens, n, threads))

    start_time = time.time()
    for t in threads:
        t.start()

    while not is_valid:
        # check if we have acheived a valid solution and/or should quit
        is_valid, no_sol = check_world_state(threads)

        if no_sol:
            break

    if is_valid:
        for t in threads:
            t.stop()

    for t in threads:
        t.join()
    stop_time = time.time()

    is_valid, no_sol = check_world_state(threads, show=True)
    runtime = stop_time - start_time
    if is_valid:
        print("INFO>> Found valid solution in {:.2f} seconds.".format(runtime))
    else:
        print("INFO>> No valid solution found. " +
              "Terminated after {.2f} seconds.".format(runtime))


def run(n_queens):

    agents = []
    for n in range(n_queens):
        agents.append(AsyncBacktrack(n_queens, n, agents))

    start_time = time.time()
    for agent in agents:
        agent.update_children(timestep=0)

    cont = True
    cnt = 0
    while cont:

        print("__Iter: {}__".format(cnt))
        is_valid, _ = check_world_state(agents, show=True)
        if is_valid:
            runtime = time.time() - start_time
            print("INFO>> Valid solution found " +
                  "in {:.2f} seconds.".format(runtime))
            break
        print("\n")

        for agent in agents:
            cont = agent.wait_for_message()
            if cont is False:
                runtime = time.time() - start_time
                print("INFO>> No valid solution found. " +
                      "Terminated after {:.2f} seconds.".format(runtime))
                break
        cnt += 1


def get_args():
    parser = ArgumentParser()
    parser.add_argument("--n-queens", type=int, default=4)
    parser.add_argument("--threaded", action="store_true", default=False)
    return parser.parse_args()


if __name__ == "__main__":

    args = get_args()

    if args.threaded:
        run_threaded(args.n_queens)
    else:
        run(args.n_queens)
