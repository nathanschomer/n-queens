from gameboard import GameBoard
from message import Message
import queue
from random import random
from threading import Thread, Event
import time


class AsyncBacktrack(Thread):

    def __init__(self, n_queens, priority, other_agents, *args, **kwargs):

        super(AsyncBacktrack, self).__init__(*args, **kwargs)

        self.timestep = 0

        self.cont = True

        # FIFO queue will store received messages
        self.msg_queue = queue.Queue()

        # agent_view & current value represented as gameboards
        self.agent_view = GameBoard(n_queens, empty=True)

        self.priority = priority
        init_location = 0   # int(n_queens*random())
        self.agent_view.move(priority, init_location)

        # agents index by priority (0 = lowest priority)
        self.other_agents = other_agents

        # list of nogoods
        self.nogoods = []

        self._stop_event = Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def get_value(self) -> int:
        """
        Return this agent's current position on the board
        """

        # intialize position to 0 if we don't have one yet
        if self.agent_view.queens.get(self.priority) is False:
            self.agent_view.queens[self.priority] = 0

        return self.agent_view.queens[self.priority]

    def update_children(self, timestep=-1):
        # print("Q{} CALL>> update_children()".format(self.priority))

        if timestep == -1:
            timestep = self.timestep + 1
        
        for n, child in enumerate(self.other_agents[self.priority+1:]):
            msg = Message(self.priority, child.priority,
                          "ok?", (self.priority, self.get_value()), timestep)
            # print("MSG>> Q{} sending ok? to Q{}".format(self.priority, child.priority))
            child.send(msg)
        return

    def run(self):

        #time.sleep(0.1) # wait for all threads to be instantiated
        self.update_children(timestep=0)

        while self.cont and not self._stop_event.is_set():
            self.cont = self.wait_for_message()

    def wait_for_message(self) -> bool:

        # print("\nQ{} CALL>> wait_for_message()".format(self.priority))

        msg = None
        cont = True
        
#        if len(self.msg_queue) > self.timestep:

        while not self.msg_queue.empty():

            msg = self.msg_queue.get(block=False)
            
            if msg.type == "ok?":
                cont = self.handle_ok(msg)
            elif msg.type == "nogood":
                cont = self.handle_nogood(msg)
            
            # print(self.agent_view)
            # no solution exists! Time to quit
            if not cont:
                return False

        self.timestep += 1
        return True

    def handle_ok(self, msg: Message) -> bool:
        # print("Q{} CALL>> handle_ok()".format(self.priority))

        assert msg.type == "ok?",\
            "msesage type is not of type 'ok?'"

        # add (A_j, d_j) to agent_view
        self.agent_view.move(msg.message[0], msg.message[1])
        # print("Agent {}: moving Q{} to col {}".format(self.priority, msg.message[0], msg.message[1]))

        return self.check_agent_view()

    def handle_nogood(self, msg: Message) -> bool:
        # print("Q{} CALL>> handle_nogood({})".format(self.priority, msg.message))

        assert msg.type == "nogood",\
            "message type is not of type 'nogood'"

        nogood = msg.message
        self.nogoods.append(nogood)

        # ignore this nogood if any of the assignments are inconsistent
        for nth_queen, loc in nogood:
            if self.agent_view.queens.get(nth_queen):
                if loc != self.agent_view.queens.get(nth_queen):
                    return True

        return self.check_agent_view(force_update=True)

    def get_filtered_nogoods(self) -> list:
        """
        Return invalid locations for current agent based on 
            current configuration and previously received nogoods
        """
        nogoods = []
        for nogood in self.nogoods:

            # remove self from current nogood
            tmp_ng = [(N, col) for (N, col) in nogood if N != self.priority]
                    
            # if config is consistent with current setup, 
            if self.agent_view.consistent_with(tmp_ng):
                nogoods.append(dict(nogood)[self.priority])

        return nogoods

    def check_agent_view(self, force_update=False) -> bool:
        # print("Q{} CALL>> check_agent_view()".format(self.priority))

        cont = True

        if not self.agent_view.is_valid() or force_update:
            nogoods = self.get_filtered_nogoods()
            # print("\t\tQ{}: Filtered nogoods = {}".format(self.priority, nogoods))
            valid_move = self.agent_view.find_valid_move(self.priority, nogoods)
            if valid_move is None:
                cont = self.backtrack()
            else:
                self.agent_view.move(self.priority, valid_move)
                self.update_children()

        return cont

    def get_nogood(self):
        nogood = self.agent_view.queens.copy()
        del nogood[self.priority]
        return list(nogood.items())

    def backtrack(self) -> bool:
        # print("Q{} CALL>> backtrack()".format(self.priority))

        cont = True

        # get last nogood added to our set of nogoods
        nogood = self.get_nogood()

        if len(nogood) == 0:
            # QUIT! there is no solution
            cont = False
        else:
            # select (A_j, d_j) in nogood where A_j has the lowest priority
            A_j = max([tmp[0] for tmp in nogood if tmp[0] != self.priority])

            # can only send nogoods to agents of lower priority
            #   should already be enforced by list comprehension above
            if A_j == self.priority:
                return True

            # send(Nogood, nogood) to A_j
            msg = Message(self.priority, A_j, "nogood", nogood, self.timestep+1)
            # print("MSG>> Q{} sending nogood to Q{}".format(self.priority, A_j))
            self.other_agents[A_j].send(msg)
            # remove (A_j, d_j) from agent_view
            self.agent_view.queens.pop(A_j)

            cont = self.check_agent_view()

        return cont

    def send(self, msg: Message):
        """
        Allows other agents to add messages to this object's message queue
        """
        # while len(self.msg_queue) <= msg.timestep:
            # start a new message queue for this timestep
        #    self.msg_queue.append(queue.Queue())
        self.msg_queue.put(msg)
