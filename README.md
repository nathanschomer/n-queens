## ABT N-Queens Solver

This repository is an asyncronous backtracking N-Queens solver. The objective of the N-Queens game is to arrange each queen such that no other queens can attack it (e.g., multiple queens must not share a row, column or diagonal). In this implementation, queens are fixed to their given row and can be moved between columns.

**usage: n_queens.py [-h] [--n-queens N_QUEENS] [--threaded]**

optional arguments:
- -h, --help
- --n-queens N_QUEENS
- --threaded



Developed with Python v3.8.6
