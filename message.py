from time import time


class Message:
    def __init__(self, origin, dest, msg_type, msg, timestep):

        assert msg_type in ["ok?", "nogood"],\
            "msg_type must be 'ok?' or 'nogood'"

        self.origin = origin
        self.destination = dest

        self.timestamp = time()
        self.timestep = timestep

        self.type = msg_type

        # tuple for "ok?" message
        # list of tuples for "nogood"
        self.message = msg

    def __str__(self):
        ret = "type:   {}\n".format(self.type)
        ret += "msg:    {}\n".format(self.message)
        ret += "origin: {}\n".format(self.origin)
        ret += "dest:   {}\n".format(self.destination)
        ret += "time:   {}\n".format(self.timestamp)
        return ret
