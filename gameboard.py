class GameBoard:

    """
    This class represents an NxN gameboard for the N Queens game.

    The objective of this game is to arrange each queen such that
    no other queens can attack it (e.g., multiple queens must not
    share a row, column or diagonal). In this implementation, queens
    are fixed to their given row and can be moved between columns.
    """

    def __init__(self, N: int, empty=False):
        """
        Create NxN gameboard with N queens
        """
        # N = num rows = num cols = num queens
        self.N = N

        # Nth queen restricted to the Nth row
        #   therefore, only store the column location
        self.queens = {}

        if not empty:
            for n in range(self.N):
                self.queens[n] = 0

    def __str__(self) -> str:
        """
        Return string representation of gameboard
        """

        tmp = ""

        for r in range(self.N):
            for c in range(self.N):
                if self.queens.get(r) == c:
                    tmp += "Q{} ".format(r)
                else:
                    tmp += "__ "
            tmp += "\n"
        #tmp += "\n"

        return tmp

    def move(self, N: int, col: int):
        """
        Move Nth queen to specificied column
        """
        assert N < self.N,\
            "N must be in range [0, {})".format(self.N)
        assert col < self.N,\
            "col must be in range [0, {})".format(self.N)

        if self.queens.get(N) is False:
            print("WARN>> New piece N = {} added to the board\n")

        self.queens[N] = col

    def is_valid_move(self, N: int, col: int) -> bool:
        """
        Check if moving piece N to col is a valid move
        """
        tmp_queens = self.queens.copy()
        self.move(N, col)
        ret = self.queen_is_valid(N)
        self.queens = tmp_queens
        return ret

    def find_valid_move(self, N, nogoods=[]) -> int:
        """
        Find valid move for piece N
        """
        # brute force... is there a better method?
        for col in range(self.N):
            
            if col in nogoods:
                continue

            if self.is_valid_move(N, col) is True:
                return col

        # no valid moves
        return None

    def queen_is_valid(self, N) -> bool:
        """
        Return True if all queens are "safe" from all others
        """
        
        # Check if one or more queens occupy the same column as this queen
        if list(self.queens.values()).count(self.queens[N]) > 1:
            return False
    
        # generate list of indecies of each queen
        queen_indicies = list(self.queens.items())

        idx1 = (N, self.queens[N])

        # iterate over list
        for idx2 in self.queens.items():
            if idx2[0] != N and abs(idx1[1]-idx2[1]) == abs(idx1[0] - idx2[0]):
                return False
            else:
                continue
        return True

    def consistent_with(self, config) -> bool:
        """
        Check if provided config is consistent with current GameBoard
        """
        for (N, col) in config:
            if self.queens.get(N) is False:
                continue
            if self.queens.get(N) != col:
                return False
        return True

    def is_valid(self) -> bool:
        """
        Return True if all queens are "safe" from all others
        """

        # Check if one or more queens occupy the same column
        if len(self.queens) is not len(set(self.queens.values())):
            return False

        # generate list of indecies of each queen
        queen_indicies = list(self.queens.items())

        # iterate over list
        for n, idx1 in enumerate(queen_indicies):
            for idx2 in queen_indicies[n+1:]:
                if abs(idx1[1]-idx2[1]) == abs(idx1[0] - idx2[0]):
                    return False
                else:
                    continue
        return True


if __name__ == "__main__":

    gb = GameBoard(4)
    gb.move(0, 1)
    gb.move(1, 3)
    gb.move(2, 0)
    gb.move(3, 2)

    assert gb.is_valid() is True,\
        "Validity check failed!"

    gb.move(3, 3)

    assert gb.is_valid() is False,\
        "Validity check failed!"

    print("INFO>> all tests passed")
